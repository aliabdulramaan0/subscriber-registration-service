package com.eskadenia.subscribersregistrationservice.config;

import com.eskadenia.subscribersregistrationservice.models.User;
import com.eskadenia.subscribersregistrationservice.services.user.UserServiceImp;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

   private final UserServiceImp userService;

    public UserDetailsServiceImp(UserServiceImp userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userService.findByUserNameAndActiveTrue(username);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + username));

        return user.map(UserDetailsImp::new).get();
    }
}
