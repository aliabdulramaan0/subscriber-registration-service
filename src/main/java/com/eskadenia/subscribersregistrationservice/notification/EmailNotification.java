package com.eskadenia.subscribersregistrationservice.notification;

import com.eskadenia.subscribersregistrationservice.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailNotification implements Notification {

    private final JavaMailSender mailSender;
    private final SpringTemplateEngine templateEngine;

    @Value("${redirectLink}")
    private String redirectLink;
    @Value("${spring.mail.from}")
    private String fromAddress ;
    @Value("${eskadenia}")
    private String senderName ;

    public EmailNotification(JavaMailSender mailSender, SpringTemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public void sendVerification(User user) {

        String toAddress =user.getEmail();
        String subject = "ESKADENIA CONFORMATION";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);


        try {
            helper.setFrom(fromAddress, senderName);
            helper.setTo(toAddress);
            helper.setSubject(subject);



        Map<String, Object> properties = new HashMap<>();
        properties.put("name", user.getUserName());


        String verifyURL = redirectLink+"/"+ user.getId() + "/verify-email" ;

        properties.put("URL", verifyURL);

        Context context = new Context();
        context.setVariables(properties);
        String html = templateEngine.process("send-email.html", context);
        helper.setText(html, true);

        mailSender.send(message);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
