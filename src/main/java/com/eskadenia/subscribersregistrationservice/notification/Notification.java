package com.eskadenia.subscribersregistrationservice.notification;

import com.eskadenia.subscribersregistrationservice.models.User;

public interface Notification {


    public void sendVerification(User user);
}
