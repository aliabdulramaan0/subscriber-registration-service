package com.eskadenia.subscribersregistrationservice.models;


import com.eskadenia.subscribersregistrationservice.models.base.BaseEntity;
import com.eskadenia.subscribersregistrationservice.models.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "subscriber")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subscriber extends BaseEntity {


    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "gender")
    private Gender gender;

    @ManyToOne(fetch = FetchType.LAZY)
    User user;
}
