package com.eskadenia.subscribersregistrationservice.models;


import com.eskadenia.subscribersregistrationservice.models.base.BaseEntity;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Data
public class Role extends BaseEntity {


    private String name;

    public Role() {
    }

    public Role(Long id, String name) {
        this.id=id;
        this.name = name;
    }
}
