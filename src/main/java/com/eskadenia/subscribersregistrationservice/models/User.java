package com.eskadenia.subscribersregistrationservice.models;


import com.eskadenia.subscribersregistrationservice.models.base.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@Data
public class User extends BaseEntity {


    @NotNull
    @Column(name = "userName",unique = true)
    private String userName;

    @NotNull
    @Column(name = "email",unique = true)
    private String email;
    @NotNull
    @Column(name = "password")
    private String password;

    private transient String confirmedPassword;
    @NotNull
    @Column(name = "active")
    private boolean active;


    @ManyToMany(fetch = FetchType.EAGER)
    private List<Role> roles= new ArrayList<>();

}
