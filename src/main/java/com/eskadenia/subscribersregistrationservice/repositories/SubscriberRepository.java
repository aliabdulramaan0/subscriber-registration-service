package com.eskadenia.subscribersregistrationservice.repositories;

import com.eskadenia.subscribersregistrationservice.models.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber,Long> {



    List<Subscriber> findAByUserUserName(String name);


}
