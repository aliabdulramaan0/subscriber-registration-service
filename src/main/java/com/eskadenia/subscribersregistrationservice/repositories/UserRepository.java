package com.eskadenia.subscribersregistrationservice.repositories;

import com.eskadenia.subscribersregistrationservice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository

public interface UserRepository extends JpaRepository<User,Long> {


    Optional<User> findByUserName(String userName);
    Optional<User> findByUserNameAndActiveTrue(String userName);
}
