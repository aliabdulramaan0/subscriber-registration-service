package com.eskadenia.subscribersregistrationservice.controllers;

import com.eskadenia.subscribersregistrationservice.models.Subscriber;
import com.eskadenia.subscribersregistrationservice.models.User;
import com.eskadenia.subscribersregistrationservice.services.subscriber.SubscriberService;
import com.eskadenia.subscribersregistrationservice.services.subscriber.SubscriberServiceImp;
import com.eskadenia.subscribersregistrationservice.services.user.UserService;
import com.eskadenia.subscribersregistrationservice.services.user.UserServiceImp;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


@Named
@ViewScoped
public class SubscriberController {


    private final SubscriberService subscriberService;
    private final UserService userService;
    private List<Subscriber>subscribers;
    private List<Subscriber> filteredSubscribers;
    private Subscriber subscriber ;
    private User loggedInUser ;
    private Subscriber updatedSubscriber ;




    public SubscriberController( SubscriberService subscriberService, UserService userService) {

        this.subscriberService = subscriberService;
        this.userService = userService;
    }




    @PostConstruct
    public void init(){
        subscriber = new Subscriber();
        loggedInUser=userService.getLoggedInUser();
        subscribers=findAllByUser(loggedInUser.getUserName());

        if(Objects.nonNull(FacesContext.getCurrentInstance())&&
                Objects.nonNull(FacesContext.getCurrentInstance().getExternalContext()) &&
                Objects.nonNull(FacesContext.getCurrentInstance().getExternalContext().getFlash().get("updatedSubscriber")) ) {
            updatedSubscriber = (Subscriber) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("updatedSubscriber");
        }
    }



    public String create(){
        subscriber.setUser(loggedInUser);
        subscriberService.save(subscriber);

        return "/subscriber-list.xhtml?faces-redirect=true";
    }

    public String update(){
        subscriberService.save(updatedSubscriber);
        return "/subscriber-list.xhtml?faces-redirect=true";
    }




    public String updateView(Subscriber subscriber){
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("updatedSubscriber", subscriber );
//        updatedSubscriber=subscriber;
        return "/edit_subscriber.xhtml?faces-redirect=true";
    }



    public List<Subscriber> findAllByUser(String userName){
        return subscriberService.findAllByUserName(userName);
    }


    public void deleteById(Long id){
        subscriberService.deleteById(id);
        subscribers=findAllByUser(loggedInUser.getUserName());
    }



    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Subscriber s = (Subscriber) value;
        return  s.getFirstName().toLowerCase().contains(filterText)
                || s.getLastName().toLowerCase().contains(filterText)
                || s.getEmail().toLowerCase().contains(filterText);

    }





    public Subscriber getUpdatedSubscriber() {
        return updatedSubscriber;
    }

    public void setUpdatedSubscriber(Subscriber updatedSubscriber) {
        this.updatedSubscriber = updatedSubscriber;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Subscriber> subscribers) {
        this.subscribers = subscribers;
    }

    public List<Subscriber> getFilteredSubscribers() {
        return filteredSubscribers;
    }

    public void setFilteredSubscribers(List<Subscriber> filteredSubscribers) {
        this.filteredSubscribers = filteredSubscribers;
    }
}
