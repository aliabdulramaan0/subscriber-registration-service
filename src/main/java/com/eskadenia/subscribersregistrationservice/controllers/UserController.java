package com.eskadenia.subscribersregistrationservice.controllers;


import com.eskadenia.subscribersregistrationservice.models.User;
import com.eskadenia.subscribersregistrationservice.services.user.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import javax.inject.Named;

@Named
@Controller
public class UserController {



    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private User user ;
//    private final EmailNotification emailNotification;


    public UserController(UserService userService, PasswordEncoder passwordEncoder) {
             this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void init() {
        user = new User();
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    public String create() {

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.create(user);

        user= new User();
       return "/subscriber-list.html?faces-redirect=true";
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
