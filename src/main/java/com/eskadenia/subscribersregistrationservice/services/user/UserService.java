package com.eskadenia.subscribersregistrationservice.services.user;

import com.eskadenia.subscribersregistrationservice.models.User;

import java.util.Optional;

public interface UserService {

    User save(User user);
    User create(User user);
     Optional<User> findByUserName(String userName);
    Optional<User> findByUserNameAndActiveTrue(String userName);
     User getLoggedInUser();

}
