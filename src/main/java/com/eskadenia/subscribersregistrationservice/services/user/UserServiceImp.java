package com.eskadenia.subscribersregistrationservice.services.user;


import com.eskadenia.subscribersregistrationservice.models.Role;
import com.eskadenia.subscribersregistrationservice.models.User;
import com.eskadenia.subscribersregistrationservice.notification.EmailNotification;
import com.eskadenia.subscribersregistrationservice.repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserServiceImp implements UserService{

    private final UserRepository userRepository;

//    private final EmailNotification emailNotification;
    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }



    public User save(User user){
        return   userRepository.save(user);
    }

    public User create(User user){
        user.getRoles().add(new Role(1l,"ROLE_ADMIN"));
        user.setActive(Boolean.TRUE);
        // emailNotification.sendVerification(user);
        return  save(user);

    }

    @Transactional(readOnly = true)
    public Optional<User> findByUserName(String userName){
        return userRepository.findByUserName(userName);
    }

    @Transactional(readOnly = true)
    public Optional<User> findByUserNameAndActiveTrue(String userName){
        return userRepository.findByUserNameAndActiveTrue(userName);
    }


    public User getLoggedInUser(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        return findByUserName(username).get();
    }
}
