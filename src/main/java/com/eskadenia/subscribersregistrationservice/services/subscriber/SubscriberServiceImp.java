package com.eskadenia.subscribersregistrationservice.services.subscriber;

import com.eskadenia.subscribersregistrationservice.models.Subscriber;
import com.eskadenia.subscribersregistrationservice.repositories.SubscriberRepository;
import com.eskadenia.subscribersregistrationservice.services.subscriber.SubscriberService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
@AllArgsConstructor
public class SubscriberServiceImp implements SubscriberService {

    private final SubscriberRepository subscriberRepository;

    public Subscriber save(Subscriber s){
        Subscriber result= subscriberRepository.save(s);
        return result;
    }


    @Transactional(readOnly = true)
    public Subscriber findById(Long id){
        Subscriber result= subscriberRepository.findById(id).orElseThrow(
                ()-> new NoSuchElementException("there is no subscriber with this id")
        );
        return result;
    }

    @Transactional(readOnly = true)
    public List<Subscriber>findAll(){
        return subscriberRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Subscriber>findAllByUserName(String userName){
        return subscriberRepository.findAByUserUserName(userName);
    }


    public void deleteById(Long id){

       subscriberRepository.deleteById(id);

    }
}
