package com.eskadenia.subscribersregistrationservice.services.subscriber;

import com.eskadenia.subscribersregistrationservice.models.Subscriber;

import java.util.List;

public interface SubscriberService {

    public Subscriber save(Subscriber s);
    public Subscriber findById(Long id);

    public List<Subscriber> findAll();
    public List<Subscriber>findAllByUserName(String userName);
    public void deleteById(Long id);


}
