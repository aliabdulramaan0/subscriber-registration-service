package com.eskadenia.subscribersregistrationservice;

import org.joinfaces.autoconfigure.viewscope.ViewScope;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

import com.google.common.collect.ImmutableMap;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@SpringBootApplication

public class SubscribersRegistrationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SubscribersRegistrationServiceApplication.class, args);

    }




//
//
//    @Bean
//    ServletRegistrationBean jsfServletRegistration (ServletContext servletContext) {
//
//        servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());
//
//
//        ServletRegistrationBean srb = new ServletRegistrationBean();
//        srb.setServlet(new FacesServlet());
//        srb.setUrlMappings(List.of("*.jsf"));
//        srb.setLoadOnStartup(1);
//        return srb;
//    }


    @Bean
    public static CustomScopeConfigurer viewScope() {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        configurer.setScopes(new ImmutableMap.Builder<String, Object>().put("view", new ViewScope()).build());
        return configurer;
    }





}
